define(['N/record', 'N/search', 'N/runtime', 'N/task', 'N/plugin', 'N/error', 'N/email', 'ajv'],
	function (record, search, runtime, task, plugin, error, email, Ajv) {
		var workQueueStatus = {
			Pending: 1,
			Processing: 2,
			Completed: 3,
			Failed: 4
		};
		var exports = {};

		/**
		 * <code>getInputData</code> event handler
		 *
		 * @governance XXX
		 * @return {*[]|Object|Search|ObjectRef} Data that will be used as input for
		 *         the subsequent <code>map</code> or <code>reduce</code>
		 * @static
		 * @function getInputData
		 */
		function getInputData() {
			var script = runtime.getCurrentScript();
			var workQueueSearchId = script.getParameter({
				name: 'custscript_work_queue_search'
			});
			if (!workQueueSearchId) throw error.create({
				name: 'MISSING_REQ_PARAM',
				message: 'The saved search script deployment param must be set to run this.'
			});
			return search.load({
				id: workQueueSearchId
			});
		}

		/**
		 * <code>map</code> event handler
		 * @governance XXX
		 * @param context
		 *        {MapContext} Data collection containing the key/value pairs to
		 *            process through the map stage
		 * @return {void}
		 * @static
		 * @function map
		 */
		function map(context) {
			var result = JSON.parse(context.value);
			context.write(result.values.custrecord_work_queue_proc_id, result.values.internalid.value);
		}

		/**
		 * <code>reduce</code> event handler
		 * @governance XXX
		 * @param context
		 *        {ReduceContext} Data collection containing the key/value pairs to
		 *          process through the reduce stage
		 * @return {void}
		 * @static
		 * @function reduce
		 */
		function reduce(context) {
			for (var i = 0; i < context.values.length - 1; i++) {
				updateQueueStatus({
					id: context.values[i],
					status: workQueueStatus.Completed
				});
			}
			var workQueueRecordId = context.values[context.values.length - 1];

			try {
				var workQueueLookup = search.lookupFields({
					type: 'customrecord_work_queue',
					id: workQueueRecordId,
					columns: ['custrecord_work_queue_type.custrecord_plugin_implementation_id', 'custrecord_work_queue_data']
				});

				var pluginId = workQueueLookup['custrecord_work_queue_type.custrecord_plugin_implementation_id'];
				var data = workQueueLookup['custrecord_work_queue_data'];

				var implementation = plugin.loadImplementation({
					type: 'customscript_plugin_work_queue_proc',
					implementation: pluginId
				});

				try {
					updateQueueStatus({
						id: workQueueRecordId,
						status: workQueueStatus.Processing
					});
					implementation.process(data, workQueueRecordId);
					updateQueueStatus({
						id: workQueueRecordId,
						status: workQueueStatus.Completed
					});
				} catch (err) {
					implementation.handleError(err);
					updateQueueStatus({
						id: workQueueRecordId,
						status: workQueueStatus.Failed,
						values: {
							custrecord_work_queue_error_message: JSON.stringify(err)
						}
					});
					throw error.create({
						name: 'PLUGIN_PROCESS_ERROR',
						message: JSON.stringify(error)
					});
				}
			} catch (err) {
				updateQueueStatus({
					id: workQueueRecordId,
					status: workQueueStatus.Failed
				});
				throw error.create({
					name: 'PLUGIN_ERROR',
					message: JSON.stringify(err)
				});
			}
		}

		/**
		 * <code>summarize</code> event handler
		 * @governance XXX
		 * @param summary
		 *        {Summary} Holds statistics regarding the execution of a map/reduce
		 *            script
		 * @return {void}
		 * @static
		 * @function summarize
		 */
		function summarize(summary) {
			var errorCount = 0;
			summary.reduceSummary.errors.iterator().each(function (key, error) {
				errorCount++;
				return true;
			});
			if (errorCount === 0 && summary.usage > 20) {
				var subject = 'Work Queue Processing Complete';
				var message = ['The process completed in', summary.seconds, 'seconds, and used', summary.usage, 'governance units.'].join(' ');
				log.audit(subject, message);
				email.send({
					author: -5,
					recipients: [],
					subject: subject,
					body: message
				});
			} else {

			}
		}

		/**
		 * @governance 10
		 * @param params
		 * @returns {internalid} The internalid of the updated record
		 */
		function updateQueueStatus(params) {
			var id = params.id;
			var status = params.status;
			var values = params.values || {};
			values['custrecord_work_queue_status'] = status;
			return record.submitFields({
				type: 'customrecord_work_queue',
				id: id,
				values: values,
				options: {
					enableSourcing: false,
					ignoreMandatoryFields: true
				}
			});
		}

		exports.getInputData = getInputData;
		exports.map = map;
		exports.reduce = reduce;
		exports.summarize = summarize;
		return exports;
	});
