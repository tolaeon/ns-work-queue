define([], function () {
	/**
	 * @fileOverview This provides an interface for work queue processor plugin implementations
	 * @author Brendan Boyd <bboyd@tolaeon.io>
	 * @version 0.1.0
	 *
	 * @NApiVersion 2.x
	 * @NModuleScope SameAccount
	 * @NScriptType plugintypeimpl
	 */
	var exports = {};

	/**
	 * <code>validate</code> event handler
	 *
	 * @governance XXX
	 * @param data {String} The work queue data document
	 * @return {Boolean} Returns a boolean representing validation pass or failure.
	 * @static
	 * @function _validate
	 */
	function _validate(data, context) {
		return true;
	}

	/**
	 * <code>getStatus</code> event handler
	 *
	 * @governance XXX
	 * @param queueRecord {Object} The JSON representation of the work queue record.
	 * @return {Object} Returns a JSON object representing the work queue process status and optional record details.
	 * @static
	 * @function _getStatus
	 */
	function _getStatus(queueRecord) {
		return {};
	}

	/**
	 * <code>process</code> event handler
	 *
	 * @governance XXX
	 * @param data {String} The work queue data document
	 * @param queueRecordId {String} The UUID associated with the work queue record.
	 * @return {Object} Optionally returns a JSON object representing any records created or processed.
	 * @static
	 * @function _process
	 */
	function _process(data, queueRecordId) {
		return {};
	}

	/**
	 * <code>handleError</code> event handler
	 *
	 * @governance XXX
	 * @param data {String} The work queue data document
	 * @static
	 * @function _handleError
	 */
	function _handleError(data) {
	}

	exports.validate = _validate;
	exports.getStatus = _getStatus;
	exports.process = _process;
	exports.handleError = _handleError;
	return exports;
});
