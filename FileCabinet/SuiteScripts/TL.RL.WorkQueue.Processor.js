define(['N/record', 'N/search', 'N/plugin', 'N/format', 'N/error'], function (record, search, plugin, format, error) {
	var workQueueStatus = {
		Pending: 1,
		Processing: 2,
		Completed: 3,
		Failed: 4
	};
	var exports = {};

	/**
	 * @governance 10
	 * @param processorId {String} The UUID processor id associated with the work queue record.
	 * @returns {boolean} Represents if the work queue records exists.
	 */
	function queueExists(processorId) {
		var queue = getWorkQueue(processorId)[0];
		return queue !== null && queue !== undefined;
	}

	/**
	 * @governance 10
	 * @param processorId {String} The UUID processor id associated with the work queue record.
	 * @returns {Array} A list of work queue type implementation id's.
	 */
	function getWorkQueue(processorId) {
		var queueRecordSearch = search.create({
			type: 'customrecord_work_queue',
			columns: ['custrecord_work_queue_type.custrecord_plugin_implementation_id'],
			filters: [
				['custrecord_work_queue_proc_id', 'is', processorId]
			]
		});

		return queueRecordSearch.run().getRange(0, 1000);
	}

	/**
	 * @governance 10
	 * @param type {string} The work queue type display name
	 * @returns {string[]} An array of plugin implementation internal id's
	 */
	function getImplementationId(type) {
		return search.create({
			type: 'customrecord_work_queue_type',
			filters: ['name', 'is', type],
			columns: [{
				name: 'internalid'
			}, {
				name: 'custrecord_plugin_implementation_id'
			}]
		}).run().getRange({start: 0, end: 1}).map(function (result) {
			return result.getValue({
				name: 'custrecord_plugin_implementation_id'
			});
		});
	}

	/**
	 * This gets a list of work queue types that can be used as a key-value lookup dictionary.
	 * @returns {Object} An object map of work queue types.
	 */
	function getWorkQueueTypes() {
		return search.create({
			type: 'customrecord_work_queue_type',
			columns: ['name', 'custrecord_plugin_implementation_id']
		})
			.run()
			.getRange({start: 0, end: 1000})
			.map(function (result) {
				return {
					id: result.id,
					name: result.getValue('name')
				};
			}).reduce(function (dictionary, value) {
				dictionary[value.name] = value.id;
				return dictionary;
			}, {});
	}

	/**
	 * <code>get</code> event handler
	 * @governance XXX
	 * @param params
	 *        {Object} The parameters from the HTTP request URL as key-value pairs;
	 * @param params.processorId
	 *        {Number} The GUID of the work queue record.
	 * @return {String|Object} Returns a String when request
	 *         <code>Content-Type</code> is <code>text/plain</code>;
	 *         returns an Object when request <code>Content-Type</code> is
	 *         <code>application/json</code>;
	 *         Representation is based on the requested work queue type.
	 * @static
	 * @function get
	 */
	function _get(params) {
		if (!params.processorId) throw error.create({
			name: 'MISSING_MANDATORY_PARAMETER',
			message: 'processorId is a required field.'
		});
		var processorId = params.processorId;
		var workQueue = getWorkQueue(processorId)[0];
		if (!workQueue) throw error.create({
			name: 'UNKOWN_PROCESSOR',
			message: ['Unknown processor id:', processorId].join(' ')
		});

		var pluginId = workQueue.getValue({
			name: 'custrecord_plugin_implementation_id',
			join: 'custrecord_work_queue_type'
		});

		var implementation = plugin.loadImplementation({
			type: 'customscript_plugin_work_queue_proc',
			implementation: pluginId
		});

		var queueRecord = record.load({
			type: 'customrecord_work_queue',
			id: workQueue.id
		});

		return implementation.getStatus(queueRecord);
	}

	/**
	 * <code>post</code> event handler
	 * @governance XXX
	 * @param request
	 *        {String|Object} The request body as a String when
	 *            <code>Content-Type</code> is <code>text/plain</code>; The
	 *            request body as an Object when request
	 *            <code>Content-Type</code> is <code>application/json</code>
	 * @param request.processorId
	 * @param request.type
	 * @return {String|Object} Returns a String when request
	 *         <code>Content-Type</code> is <code>text/plain</code>;
	 *         returns an Object when request <code>Content-Type</code> is
	 *         <code>application/json</code>
	 * @static
	 * @function post
	 */
	function post(request) {
		if (!request.processorId) throw error.create({
			name: 'MISSING_MANDATORY_PARAMETER',
			message: 'processorId is a required param.'
		});
		if (!request.type) throw error.create({
			name: 'MISSING_MANDATORY_PARAMETER',
			message: 'type is a required param'
		});
		if (queueExists(request.processorId)) throw error.create({
			name: 'EXISTING_RECORD',
			message: 'A record with this processorId already exists'
		});
		if (request.type) {
			var types = getWorkQueueTypes();
			var implementationId = getImplementationId(request.type)[0];
			if (implementationId) {
				var implementation = plugin.loadImplementation({
					type: 'customscript_plugin_work_queue_proc',
					implementation: implementationId
				});
				if (implementation.validate(request)) {
					var date = format.format({
						value: new Date(),
						type: format.Type.DATETIME
					});
					var workQueue = record.create({
						type: 'customrecord_work_queue',
						isDynamic: true
					});
					workQueue.setValue({
						fieldId: 'name',
						value: [request.type, date].join(' - ')
					});
					workQueue.setValue({
						fieldId: 'custrecord_work_queue_data',
						value: JSON.stringify(request)
					});
					workQueue.setValue({
						fieldId: 'custrecord_work_queue_proc_id',
						value: request.processorId
					});
					workQueue.setValue({
						fieldId: 'custrecord_work_queue_status',
						value: workQueueStatus.Pending
					});
					workQueue.setValue({
						fieldId: 'custrecord_work_queue_type',
						value: types[request.type]
					});
					return {
						processorId: request.processorId,
						id: workQueue.save(),
						status: 'Success'
					};
				} else {
					throw error.create({
						name: 'VALIDATION_ERROR',
						message: 'Failed validation'
					});
				}
			} else {
				throw error.create({
					name: 'PLUGIN_ERROR',
					message: 'Unknown plugin type'
				});
			}
		}
	}

	/**
	 * <code>put</code> event handler
	 * @governance XXX
	 * @param request
	 *        {String|Object} The request body as a String when
	 *            <code>Content-Type</code> is <code>text/plain</code>; The
	 *            request body as an Object when request
	 *            <code>Content-Type</code> is <code>application/json</code>
	 * @return {String|Object} Returns a String when request
	 *         <code>Content-Type</code> is <code>text/plain</code>;
	 *         returns an Object when request <code>Content-Type</code> is
	 *         <code>application/json</code>
	 * @static
	 * @function put
	 */
	function put(request) {
		throw 'Put not supported';
	}

	/**
	 * <code>delete</code> event handler
	 * @governance XXX
	 * @param params
	 *        {Object} The parameters from the HTTP request URL as key-value pairs
	 * @return {String|Object} Returns a String when request
	 *         <code>Content-Type</code> is <code>text/plain</code>;
	 *         returns an Object when request <code>Content-Type</code> is
	 *         <code>application/json</code>
	 * @static
	 * @function _delete
	 */
	function _delete(params) {
		throw 'Delete not supported';
	}

	exports.get = _get;
	exports.post = post;
	exports.put = put;
	exports.delete = _delete;
	return exports;
});
