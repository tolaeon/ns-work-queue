# ns-work-queue

This message queue design provides a method for queueing data of varying types, asynchronously processing the data, and notifying a third party API of success or failure.